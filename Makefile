
PKG = web2desk

INFO = $(shell uname -a)
OST = $(shell lsb_release -d | sed 's|Description:\s*||')
OS = $(shell lsb_release -d | sed 's|Description:\s*||'|cut -d\  -f1)

ifeq ($(OS),Ubuntu)
	PACKAGE_MANAGER = apt
endif

ifeq ($(OS),Debian)
	PACKAGE_MANAGER = apt
endif

ifeq ($(OS),Arch)
	PACKAGE_MANAGER = pacman
endif

ifeq ($(PACKAGE_MANAGER),)
$(error >   Unsupported OS: ${OST})
endif

all: info web2desk


web2desk:
	@echo -e "\n\e[1;33m[+]\e[0m Installing ${PKG}...\n"
	@cp -rvf ${PKG}/usr ${PKGDIR}/ || true
	@[ ! -d ${PKGDIR}/usr/share/${PKG} ] && echo -e "\n\e[1;31m[-]\e[0m Please ensure you have sufficient premission to install ${PKG}." || true

clean:
	@echo -e "\n\e[1;33m[+]\e[0m Cleaning up...\n"
	@rm -fv ${PKGDIR}/usr/bin/${PKG} || true
	@rm -rfv ${PKGDIR}/usr/share/${PKG} || true
	@rm -rfv ${PKGDIR}/usr/share/licenses/${PKG} || true
	@rm -rfv ${PKGDIR}/usr/share/doc/${PKG} || true
	@[ -d ${PKGDIR}/usr/share/${PKG} ] && echo -e "\n\e[1;31m[-]\e[0m Please ensure you have sufficient premission to install ${PKG}." || true


install: all

uninstall: clean

info:
	@echo -e "\n\e[1;33m[*]\e[0m ${INFO}"

.PHONY: all web2desk install clean uninstall info

